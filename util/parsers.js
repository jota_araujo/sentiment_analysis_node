const _ = require('lodash');
const fs = require('fs');

const stop_words = ['corinthians'];
const filepath = `${__dirname}/stop_words.txt`;

function strip_urls(input_str) {
  return input_str.replace(/https?:\/\/[^\.]+\.\S{2,}/g, '');
}

function strip_stop_words(arr) {
  return _.reject(arr, function (o) { return _.includes(stop_words, o); });
}

const lineReader = require('readline').createInterface({
  input: fs.createReadStream(filepath),
});

lineReader.on('line', function (line) {
  if (line != null && line.length > 0) stop_words.push(line);
});

function naive_tokenize(input) {
  const str1 = strip_urls(input);
  const tokenized = str1.replace(/[^a-zA-Z\u00E0-\u00FC:\/. ]/gi, '').toLowerCase();
  const arr = strip_stop_words(tokenized.split(' '));
  return arr;
}


// exports
exports.strip_urls = strip_urls;
exports.stop_words = strip_stop_words;
exports.naive_tokenize = naive_tokenize;