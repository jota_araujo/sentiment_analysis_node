const express = require('express');
const path = require('path');
// const favicon = require('serve-favicon');
const logger = require('winston');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const naive_tokenize = require('./util/parsers').naive_tokenize;

const routes = require('./routes/index');
const users = require('./routes/users');
const chalk = require('chalk');
const sentiment = require('sentiment-ptbr');

const app = express();
const TwitService = require('./services/twit_service');
const credentials = require('./credentials.js');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function (__req, __res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, __req, res, __next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err,
    });
  });
}

// production error handler no stacktraces leaked to user
app.use(function (err, __req, res, __next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {},
  });
});

function processTweet(tweet, _log) {
  const text = tweet.text;
  _log('--------------------------------------------------');
  _log(text);

  // custom tokenization
  const tokenized = naive_tokenize(text);
  // _log('tokenized:', tokenized.join(', '));

  // sentiment analysis
  const result = sentiment(tokenized.join(' '));
  _log(`positive words: ${chalk.green(result.positive)}  negative words: ${chalk.red(result.negative)}`);

  // pretty print to the console
  let score_label = chalk.bold.white('neutral');
  if (result.score > 2) score_label = chalk.inverse.bold.green('positive');
  if (result.score < -2) score_label = chalk.inverse.bold.red('negative');
  _log(`sentiment: ${score_label}`);

}


const twitService = new TwitService(credentials.consumer_key, credentials.consumer_secret,
 credentials.access_token, credentials.access_token_secret);
twitService.startStream({ track: ['corinthians', 'timao'],
                            language: ['pt'] },
                            processTweet, logger.info);

module.exports = app;
