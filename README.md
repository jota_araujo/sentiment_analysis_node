# Description:

Sentiment analysis with Node.js.  
Relies on (https://github.com/thisandagain/sentiment) for the analysis but adds custom tokenization and url detection.

# Screenshots:

![.](https://bytebucket.org/jota_araujo/sentiment_analysis_node/raw/54f67bea9cfb7c58b9358c062f9dab0767963d06/sentiment_analysis_screenshot1.jpg)